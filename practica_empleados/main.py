from persona import Persona
from jefe import Jefe
from empleado import Empleado

if __name__ == "__main__":
    empleadoPepe = Empleado("Pepe", 20000)
    jefeAna = Jefe("Ana", 30000, 1000)
    
    total = empleadoPepe.calculo_impuestos() + jefeAna.calculo_impuestos()
    
    print(empleadoPepe)
    print(jefeAna)
    
    print("Los impuestos a pagar en total son {:.2f} euros".format(total))
