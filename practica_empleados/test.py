'''Probando el archivo empleados.py'''

import unittest
from empleado import Empleado
from jefe import Jefe
from persona import Persona

class TestPersona(unittest.TestCase):
    def test_construir(self):
        e1 = Persona("Juanito", 1234)
        self.assertEqual(e1.nif, 1234)
class TestJefe(unittest.TestCase):
    def test_construir(self):
        e1 = Jefe("Paquito", 1000, 20, 1235)
        self.assertEqual(e1.bonus, 20)
class TestEmpleado(unittest.TestCase):
    def test_construir(self):
        e1 = Empleado("nombre",5000)
        self.assertEqual(e1.nomina, 5000)
    def test_impuestos(self):
        e1 = Empleado("nombre",5000)
        self.assertEqual(e1.calculo_impuestos(), 1500)
    def test_str(self):
        e1 = Empleado("pepe",50000)
        self.assertEqual("El empleado pepe debe pagar 15000.00", e1.__str__())
    def test_nombre(self):
            e1 = Empleado("pepe",5000)
            self.assertEqual(e1.nombre, 'pepe')
    def test_nomina_negativa(self):
        e1 = Empleado("pepe",-10)
        self.assertEqual(e1.nomina, 0.00)
            
if __name__ == "__main__":
    unittest.main()