from empleado import Empleado

class Jefe(Empleado):
    def __init__(self, nom, sal, extra = 0, nif = 0):
        super().__init__(nom, sal, nif)
        self.bonus = extra
    def calculo_impuestos (self):
        return (self.nomina + self.bonus)*0.30
    def __str__(self):
        return "El jefe {name} debe pagar {tax:.2f}".format(name= self.nombre,
                tax = self.calculo_impuestos())