from persona import Persona

class Empleado(Persona):
    def __init__(self, nom, sal, nif = 0):
        super().__init__(nom, nif)
        if sal > 0.0:
            self.nomina = sal
        else:
            self.nomina = 0.0
    def calculo_impuestos(self):
            return self.nomina*0.30
    def __str__(self):
        return "El empleado {name} debe pagar {tax:.2f}".format(name=self.nombre,
                tax=self.calculo_impuestos())